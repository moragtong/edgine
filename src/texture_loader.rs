use super::*;
use std::io::Read;

#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
struct PixelFormat {
	pub size:			u32,
	pub flags:			u32,
	pub four_cc:		[u8; 4],
	pub rgb_bit_count:	u32,
	pub red_bit_mask:	u32,
	pub green_bit_mask:	u32,
	pub blue_bit_mask:	u32,
	pub alpha_bit_mask:	u32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
struct Header {
	pub magic:					u32,
	pub size:					u32,
	pub flags:					u32,
	pub height:					u32,
	pub width:					u32,
	pub pitch_or_linear_size:	u32,
	pub depth:					u32,
	pub mipmap_count:			u32,
	pub reserved:				[u32; 11],
	pub pixel_format:			PixelFormat,
	pub caps:					u32,
	pub caps2:					u32,
	pub caps3:					u32,
	pub caps4:					u32,
	pub reserved2:				u32,
}

pub struct TextureLoader {
	pub file:			std::fs::File,
	pub size:			u64,
	pub extent:			u32,
}

impl TextureLoader {
	pub fn new(name: impl AsRef<std::path::Path>) -> std::io::Result<Self> {
		let mut file = std::fs::File::open(name)?;
		let size = file.metadata()?.len() - std::mem::size_of::<Header>() as u64;

		let mut header: Header = unsafe { std::mem::uninitialized() };

		file.read_exact(unsafe { std::slice::from_raw_parts_mut(&mut header as *mut _ as _, std::mem::size_of::<Header>()) })?;

		debug_assert!(header.width == header.height);

		println!("{:?}", unsafe { std::str::from_utf8_unchecked(&header.pixel_format.four_cc) });
		Ok(Self { file, size, extent: header.width })
	}
}