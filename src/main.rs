extern crate vulkano;
extern crate vulkano_win;
extern crate winit;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate bincode;
extern crate cgmath;

mod texture_loader;

use std::io::Read;
use std::convert::Into;
use std::iter::FromIterator;
use vulkano::command_buffer::pool::CommandPool;
use vulkano::memory::pool::MemoryPool;
use vulkano::memory::pool::MemoryPoolAlloc;

const SWAPCHAIN_IMAGE_COUNT: usize = 3;
const TEXTURE_COUNT: usize = 3;
const TEXTURE_COMPRESSION_RATIOS: [usize; TEXTURE_COUNT] = [2, 2, 1];
const TEXTURE_FORMATS: [vulkano::format::Format; TEXTURE_COUNT] = [
	vulkano::format::Format::BC1_RGBUnormBlock,
	vulkano::format::Format::BC4UnormBlock,
	vulkano::format::Format::BC5UnormBlock,
];

#[derive(Serialize, Deserialize, Debug)]
struct MapObject {
	location:	[[f32; 4]; 4],
	name:		String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ModelLoader {
	textures:	[String; TEXTURE_COUNT],
	vertices:	Vec<f32>,
	indices:	Vec<u32>,
}

#[derive(Default)]
struct PerModel {
	locations:	Vec<[[f32; 4]; 4]>,
	layers:		[f32; TEXTURE_COUNT],
}

struct VerticesIndices {
	vertices:	Vec<f32>,
	indices:	Vec<u32>,
}

#[derive(Default)]
struct Texture {
	texture_loaders:	Vec<texture_loader::TextureLoader>,
	image:				Option<std::sync::Arc<vulkano::image::ImmutableImage<vulkano::format::Format>>>,
}

struct PerInstance {
	view_model:						[[f32; 4]; 4],
	proj_view_model:				[[f32; 4]; 4],
	transpose_inverse_view_model:	[[f32; 4]; 4], // contains a mat3 and a float (texture layer)
}

fn find_physical_device<'inst>(instance: &'inst std::sync::Arc<vulkano::instance::Instance>, features: &vulkano::instance::Features)
-> Option<(vulkano::instance::PhysicalDevice<'inst>, vulkano::instance::DeviceExtensions, Vec<(vulkano::instance::QueueFamily<'inst>, f32)>)> {
	vulkano::instance::PhysicalDevice::enumerate(&instance).filter_map(|physical| {
		let supported_extensions = vulkano::instance::DeviceExtensions::supported_by_device(physical);
		if supported_extensions.khr_swapchain && physical.supported_features().superset_of(features) {
			let graphics_family = physical.queue_families()
				.find(|queue_family| queue_family.supports_graphics())?;

			let transfer_family = physical.queue_families()
				.find(|queue_family| queue_family.supports_transfers() && !queue_family.supports_graphics())?;

			Some((physical, supported_extensions, [(graphics_family, 1.0), (transfer_family, 1.0)].as_ref().into()))
		} else {
			None
		}
	}).next()
}

pub fn create_device(instance: &std::sync::Arc<vulkano::instance::Instance>)
-> Result<(std::sync::Arc<vulkano::device::Device>, std::sync::Arc<vulkano::device::Queue>, std::sync::Arc<vulkano::device::Queue>), Box<std::error::Error>> {
	let features = vulkano::instance::Features {
		sampler_anisotropy:		true,
		texture_compression_bc:	true,
		image_cube_array:		true,
		..vulkano::instance::Features::none()
	};

	let (physical, supported_extensions, queue_families) = find_physical_device(&instance, &features)
		.ok_or("No physical devices available")?;

	println!("Physical Device: {}", physical.name());

	let (device, mut queues_iter) = vulkano::device::Device::new(physical, &features, &supported_extensions, queue_families)?;

	let graphics_queue = queues_iter.next()
		.ok_or("Could not get graphics queue handle")?;

	let transfer_queue = queues_iter.next()
		.ok_or("Could not get transfer queue handle")?;

	Ok((device, graphics_queue, transfer_queue))
}

pub struct Engine {
	events_loop:			winit::EventsLoop,
	extent:					[u32; 2],

	transfer_queue:			std::sync::Arc<vulkano::device::Queue>,
	transfer_cmd_buffer:	vulkano::command_buffer::pool::standard::StandardCommandPoolBuilder,

	models:					Vec<PerModel>,
	graphics_queue:			std::sync::Arc<vulkano::device::Queue>,
	graphics_cmd_buffers:	Vec<vulkano::command_buffer::pool::standard::StandardCommandPoolBuilder>,

	swapchain:				std::sync::Arc<vulkano::swapchain::Swapchain<winit::Window>>,
	swapchain_images:		Vec<std::sync::Arc<vulkano::image::swapchain::SwapchainImage<winit::Window>>>,
	semaphores:				Vec<(vulkano::sync::Semaphore, vulkano::sync::Semaphore)>,
	fences:					Vec<vulkano::sync::Fence>,
}

impl Engine {
	pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
		let events_loop = winit::EventsLoop::new();

		let window = winit::Window::new(&events_loop)?;

		let extent = {
			let extent: (u32, u32) = window.get_inner_size()
				.ok_or("Lost window")?.into();
			[extent.0, extent.1]
		};

		let instance = vulkano::instance::Instance::new(None, &vulkano_win::required_extensions(), None)?;

		let (device, graphics_queue, transfer_queue) = create_device(&instance)?;

		let (swapchain, swapchain_images) = vulkano::swapchain::Swapchain::new(device.clone(),
			vulkano_win::create_vk_surface(window, instance)?,
			SWAPCHAIN_IMAGE_COUNT as _, vulkano::format::B8G8R8A8Unorm, extent, 1,
			vulkano::image::ImageUsage {
				color_attachment:	true,
				..vulkano::image::ImageUsage::none()
			}, &graphics_queue, Default::default(), vulkano::swapchain::CompositeAlpha::Opaque,
			vulkano::swapchain::PresentMode::Immediate, Default::default(), Default::default()
		)?;

		let transfer_cmd_pool = std::sync::Arc::new(vulkano::command_buffer::pool::StandardCommandPool::new(device.clone(), transfer_queue.family()));
		let graphics_cmd_pool = std::sync::Arc::new(vulkano::command_buffer::pool::StandardCommandPool::new(device.clone(), graphics_queue.family()));

		Ok(Self { events_loop, extent, transfer_queue, graphics_queue, swapchain, swapchain_images,
			models:					Default::default(),

			transfer_cmd_buffer:	transfer_cmd_pool.alloc(false, 1)?.next()
				.ok_or("Could not acquire transfer command buffer")?,

			graphics_cmd_buffers:	graphics_cmd_pool.alloc(false, SWAPCHAIN_IMAGE_COUNT as _)?.collect(),

			semaphores:				Result::<_, Box<dyn std::error::Error>>::from_iter((0..SWAPCHAIN_IMAGE_COUNT)
				.map(|_| Ok((vulkano::sync::Semaphore::alloc(device.clone())?, vulkano::sync::Semaphore::alloc(device.clone())?))))?,
			fences:					Result::<_, Box<dyn std::error::Error>>::from_iter((0..SWAPCHAIN_IMAGE_COUNT)
				.map(|_| Ok(vulkano::sync::Fence::alloc_signaled(device.clone())?)))?,
		})
	}

	fn load(&mut self, map_objects: Vec<MapObject>, sampler: &std::sync::Arc<vulkano::sampler::Sampler>) -> Result<(), Box<dyn std::error::Error>> {
		let (mut per_models, model_loaders) = {
			let mut model_locations = std::collections::HashMap::<_, Vec<_>>::new();
			for map_object in map_objects.into_iter() {
				model_locations.entry(map_object.name).or_default().push(map_object.location);
			}

			(Vec::from_iter(model_locations.values_mut().map(|locations| PerModel {
				locations:	std::mem::replace(locations, Default::default()),
				..Default::default()
			})),
			Result::<Vec<ModelLoader>, Box<dyn std::error::Error>>::from_iter(model_locations.into_iter()
				.map(|(name, _)| Ok(bincode::deserialize_from(std::fs::File::open(name)?)?)))?)
		};

		let vertex_sum: usize = model_loaders.iter().map(|model_loader| model_loader.vertices.len() * std::mem::size_of::<f32>()).sum();
		let index_sum: usize = model_loaders.iter().map(|model_loader| model_loader.indices.len() * std::mem::size_of::<u32>()).sum();

		let mut texture_extent_layer_maps = [(std::collections::HashMap::new(), std::collections::HashMap::new()), Default::default(), Default::default()];

		let mut per_model_map = std::collections::HashMap::new();

		for (model_loader, mut per_model) in model_loaders.into_iter().zip(per_models) {
			let mut extents = [0; TEXTURE_COUNT];

			for (((name, (texture_map, extent_layer_map)), &mut ref mut extent), &mut ref mut layer) in model_loader.textures.iter()
			.zip(texture_extent_layer_maps.iter_mut()).zip(extents.iter_mut()).zip(per_model.layers.iter_mut()) {
				let mut res = Ok(());
				let (eextent, elayer) = *extent_layer_map.entry(name.clone()).or_insert_with(|| {
					match texture_loader::TextureLoader::new(name) {
						Ok(texture_loader) => {
							let entry = texture_map.entry(texture_loader.extent).or_insert_with(Texture::default);
							let ret = (texture_loader.extent, (entry.texture_loaders.len() - 1) as _);
							entry.texture_loaders.push(texture_loader);
							ret
						}
						Err(err) => {
							res = Err(err);
							(0, 0.)
						}
					}
				});
				res?;
				*extent = eextent;
				*layer = elayer;
			}

			let (eper_models, evertices_indices_s) = per_model_map.entry(extents).or_insert_with(|| (Vec::new(), Vec::new()));
			eper_models.push(per_model);
			evertices_indices_s.push((model_loader.vertices, model_loader.indices));
		}

		let textures_sum = texture_extent_layer_maps.iter().zip(&TEXTURE_COMPRESSION_RATIOS).fold(0,
			|acc, ((texture_map, _), ratio)| acc + texture_map.iter().fold(0,
				|acc, (&extent, texture)| acc + texture.texture_loaders.len() * extent as usize * extent as usize / ratio));

		let host_memory_size = textures_sum as usize + index_sum + vertex_sum;

		unsafe {
			let (upload, reqs) = vulkano::buffer::sys::UnsafeBuffer::new::<std::iter::Empty<_>>(self.transfer_queue.device().clone(),
				host_memory_size,
				vulkano::buffer::BufferUsage::transfer_source(),
				vulkano::sync::Sharing::Exclusive,
				vulkano::buffer::sys::SparseLevel::none())?;

			let pool = vulkano::device::Device::standard_pool(self.transfer_queue.device());
			let alloc = pool.alloc_from_requirements(&reqs,
				vulkano::memory::pool::AllocLayout::Linear,
				vulkano::memory::pool::MappingRequirement::Map,
				vulkano::memory::DedicatedAlloc::Buffer(&upload),
				|memtype| if memtype.is_host_visible() {
					vulkano::memory::pool::AllocFromRequirementsFilter::Preferred
				} else {
					vulkano::memory::pool::AllocFromRequirementsFilter::Forbidden
				})?;

			let map = alloc.mapped_memory()
				.ok_or("Could not get mapped memory handle.")?;

			{
				let mut access = map.read_write::<[_]>(0..host_memory_size);

				let mut pos = 0;

				let descriptor_sets_writes = Result::<Vec<_>, Box<dyn std::error::Error>>::from_iter(per_model_map.iter().map(|(extents, (per_models, _))| {
					let mut descriptor_writes: [_; TEXTURE_COUNT] = std::mem::uninitialized();
					for ((((texture_map, _), extent), &mut ref mut descriptor_write), format) in
					texture_extent_layer_maps.iter_mut().zip(extents).zip(descriptor_writes.iter_mut()).zip(&TEXTURE_FORMATS) {
						let texture = texture_map.get_mut(extent).unwrap();
						let mut option_texture_image: Option<Result<_, Box<dyn std::error::Error>>> = texture.image.take().map(|image| Ok(image));
						let texture_image = option_texture_image.unwrap_or_else(|| {
							let (image, init) = vulkano::image::ImmutableImage::uninitialized(
								self.transfer_queue.device().clone(),
								vulkano::image::Dimensions::Dim2dArray {
									width:			*extent,
									height:			*extent,
									array_layers:	texture.texture_loaders.len() as _,
								},
								*format,
								1,
								vulkano::image::ImageUsage {
									transfer_destination:	true,
									sampled:				true,
									..vulkano::image::ImageUsage::none()
								},
								vulkano::image::ImageLayout::Undefined,
								std::iter::once(self.graphics_queue.family()))?;

							for texture_loader in texture.texture_loaders.iter_mut() {
								pos += texture_loader.file.read(access.get_unchecked_mut(pos..))?
							}
							Ok(image)
						})?;
						std::ptr::write(descriptor_write, texture_image.clone());
						texture.image = Some(texture_image);
					}
					Ok(descriptor_writes)
				}));
			}

			//let descriptor_writes = Vec::with_capacity(per_model_map.len());

			upload.bind_memory(alloc.memory(), alloc.offset())?;
		}

		Ok(())
	}

	fn run(&mut self) -> Result<(), Box<dyn std::error::Error>> {
		let mut should_close = false;

		for ((acquire_semaphore, submit_semaphore), fence) in self.semaphores.iter().zip(&self.fences).cycle() {
			self.events_loop.poll_events(|event| match event {
				winit::Event::WindowEvent { event: winit::WindowEvent::CloseRequested, .. } =>
					should_close = true,
				_ => ()
			});

			if should_close {
				break
			}

			fence.wait(None)?;

			unsafe {
				vulkano::swapchain::acquire_next_image_raw(&self.swapchain, None, Some(acquire_semaphore), None)?;
			}

			
		}
		Ok(())
	}
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let mut engine = Engine::new()?;
	engine.run()?;

	Ok(())
}